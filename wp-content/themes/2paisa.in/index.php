<?php get_header(); ?>
<?php require_once('featured-post.php'); ?>

<section class="section wb">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
                <div class="page-wrapper">
                    <div class="blog-list clearfix">
                    <?php 

                    // $post_custom = new WP_Query(array(
                    //     'posts_per_page' =>2,   
                    // ));

                    ?>
                    <?php while(have_posts()): the_post(); ?>

                    
                    <div class="blog-box row">
                            <div class="col-md-4">
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>" title="">
                                        <img src="<?php the_post_thumbnail_url(array(400,400)); ?>" alt="" class="img-fluid">
                                        <div class="hovereffect"></div>
                                    </a>
                                </div><!-- end media -->
                            </div><!-- end col -->
                            <div class="blog-meta big-meta col-md-8">
                            <?php 
                            $category =  get_the_category(); 
                            foreach($category as $categories){
                                $cat_name = $categories->cat_name;
                                $cat_id = $categories->cat_ID;
                            }
                            ?>
                                <span class="bg-aqua"><a href="<?php echo get_category_link($cat_id); ?>" title=""><?php echo $cat_name; ?></a></span>
                                <h4><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h4>
                                <p><?php the_excerpt(); ?></p>
                                <small><a href="<?php the_permalink(); ?>" title=""><i class="fa fa-eye"></i> 1887</a></small>
                                <small><a href="<?php the_permalink(); ?>" title=""><?php the_date(); ?></a></small>
                                <small><a href="<?php the_author_link(); ?>" title="">by <?php the_author(); ?></a></small>
                            </div>
                        </div>
                        <hr class="invis">
                        <?php endwhile; ?>
                    </div>
                </div>
                <hr class="invis">

                



                <div class="row">
                    <div class="d-flex col-md-12 justify-content-center">
                            <?php the_posts_pagination(array(
                                'prev_text' => 'Previous',
                                'next_text' => 'Next',
                                'screen_reader_text'=> ' '
                                )); 
                            ?>    
                    </div>
                </div>
            </div>
            <?php require_once('sidebar.php'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>