<?php
global $paisa;
$logo = $paisa['header_2_logo'];
$footer_copyright = $paisa['footer_copyright'];


?>

<footer class="footer py-5 mb-0">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="newsletter-widget text-center">
                    <form class="form-inline">
                        <!-- <input type="text" class="form-control" placeholder="Enter your email address">
                        <button type="submit" class="btn btn-primary">Subscribe <i class="fa fa-envelope-open-o"></i></button> -->
                        <?php echo do_shortcode('[contact-form-7 id="38" title="subsribe"]'); ?>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row pt-3">
            <div class="col-md-3 d-flex justify-content-start align-items-center">
                <div class="copyright"><?php echo $footer_copyright;  ?></div>
            </div>
            <div class="col-md-6 d-flex justify-content-center align-items-center">
                <div>
                    <a href="#"><p class="h1 font-weight-bold font-italic" style="font-family: 'Sofia';font-size: 45px;"><?php echo $logo;  ?></p></a>
                </div>
            </div>
        </div>
    </div>
</footer>

    
</div>
<script src="<?php bloginfo('template_directory')?>/assets/js/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory')?>/assets/js/tether.min.js"></script>
<script src="<?php bloginfo('template_directory')?>/assets/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory')?>/assets/js/custom.js"></script>
<?php wp_footer(); ?>
</body>
</html>