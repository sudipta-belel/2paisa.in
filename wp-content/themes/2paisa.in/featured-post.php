<?php 
$the_query = new WP_Query( array(
            'post_type' => 'post', 
            'posts_per_page' => 3,
            ));
?>
<section class="section first-section">
<div class="container-fluid">
    <div class="row">              
<?php while($the_query->have_posts()): $the_query->the_post(); ?>
    <div class="col-md-4">
        <div class="masonry-box post-media">
                <img src="<?php the_post_thumbnail_url(array(400,400)); ?>" alt="" class="img-fluid">
                <div class="shadoweffect">
                <div class="shadow-desc">
                    <div class="blog-meta">
                        <?php $category =  get_the_category(); 
                        foreach($category as $categories){
                            $cat_name = $categories->cat_name;
                            $cat_id = $categories->cat_ID;
                            ?>
                            <span class="bg-aqua"><a href="<?php echo get_category_link($cat_id); ?>" title=""><?php echo $cat_name;  ?></a></span>
                            <?php
                        }
                        ?>
                        <h4><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h4>
                        <small><a href="<?php the_permalink(); ?>" title=""><?php echo get_the_date(); ?></a></small>
                        <small><a href="<?php the_author_link(); ?>" title="">by <?php the_author(); ?></a></small>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php wp_reset_query(); ?>
        </div>
    </div>
</section>