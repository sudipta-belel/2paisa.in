<!DOCTYPE html>
<html lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>2paisa - Finance Investment & Mutual Fund</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="shortcut icon" href="<?php bloginfo('template_directory')?>/assets/images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?php bloginfo('template_directory')?>/assets/images/apple-touch-icon.png">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'> 
    <link href="<?php bloginfo('template_directory')?>/assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory')?>/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory')?>/assets/style.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory')?>/assets/css/responsive.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory')?>/assets/css/colors.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory')?>/assets/css/version/garden.css" rel="stylesheet">
    <link href="<?php bloginfo('template_directory')?>/style.css" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body>
<?php 
global $paisa;
$logo = $paisa['header_2_logo']; 
$tagline = $paisa['header_2_tagline'];
$facebook = $paisa['top_header_fb_link'];
$youtube = $paisa['top_header_youtube_link'];
$pinterest = $paisa['top_header_piterest_link'];
$twitter = $paisa['top_header_twitter_link'];
?>
    <div id="wrapper">
        <!-- Topbar Section -->
        <div class="topbar-section">
            <div class="container-fluid">
                <div class="row">
                        <div class="topsocial">
                            <a href="<?php echo $facebook; ?>" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>
                            <a href="<?php echo $youtube; ?>" data-toggle="tooltip" data-placement="bottom" title="Youtube"><i class="fa fa-youtube"></i></a>
                            <a href="<?php echo $pinterest; ?>" data-toggle="tooltip" data-placement="bottom" title="Pinterest"><i class="fa fa-pinterest"></i></a>
                            <a href="<?php echo $twitter; ?>" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
                        </div>
                </div>
            </div>
        </div>
        <!-- Header Section -->
        <div class="header-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                    <a href="#"><p class="h1 font-weight-bold font-italic" style="font-family: 'Sofia';font-size: 45px;"><?php echo $logo; ?></p></a>
                    <p><?php echo $tagline; ?></p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navigation -->
        <header class="header">
            <div class="container">
                <nav class="navbar navbar-inverse navbar-toggleable-md">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#Forest Timemenu" aria-controls="Forest Timemenu" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-md-center" id="Forest Timemenu">
                        <?php
                            wp_nav_menu( array(
                            'theme_location' => 'header-menu',
                            'menu_class' => 'navbar-nav ',
                            'container' => 'ul',
                            )); 
                        ?>

                        <!-- <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link color-green-hover" href="garden-index.html">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link color-green-hover" href="garden-category.html">Sensex</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link color-green-hover" href="garden-category.html">Mutual Fund</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link color-green-hover" href="garden-category.html">Gold Silver</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link color-green-hover" href="garden-category.html">Investing</a>
                            </li>  
                            <li class="nav-item">
                                <a class="nav-link color-green-hover" href="garden-contact.html">Contact</a>
                            </li>
                        </ul> -->
                    </div>
                </nav>
            </div>
        </header>
        <!-- Recent Posts -->

