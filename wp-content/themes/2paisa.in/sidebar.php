<?php 
global $paisa;
$search_text = $paisa['sidebar_1_search'];
$recent_post_text = $paisa['sidebar_1_recent_posts'];
$gallery = $paisa['sidebar_1_gallery'];
$popular_categories_text = $paisa['sidebar_1_categories'];
?>



<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                        <div class="sidebar">
                            <div class="widget">
                                <h2 class="widget-title"><?php echo $search_text; ?></h2>
                                <form class="form-inline search-form">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Search on the site">
                                    </div>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                </form>
                            </div><!-- end widget -->

                            <div class="widget">
                                <h2 class="widget-title"><?php echo $recent_post_text; ?></h2>
                                <div class="blog-list-widget">
                                    <div class="list-group">
                                    <?php $the_query = new WP_Query( array(
                                                'post_type' => 'post', 
                                                'posts_per_page' => 5,
                                                ));
                                        ?>
                                        <?php while($the_query->have_posts()): $the_query->the_post(); ?>
                                                <a href="garden-single.html" class="list-group-item list-group-item-action flex-column align-items-start">
                                            <div class="w-100 justify-content-between">
                                                <img src="<?php the_post_thumbnail_url(array(400,400)); ?>" alt="" class="img-fluid float-left">
                                                <h5 class="mb-1"><?php the_title(); ?></h5>
                                                <small><?php echo get_the_date(); ?></small>
                                            </div>
                                        </a>
                                        <?php endwhile; ?>
                                    </div>
                                </div>
                            </div>

                            <div class="widget">
                                <h2 class="widget-title"><?php echo $gallery; ?></h2>
                                <div class="banner-spot clearfix">
                                    <div class="banner-img">
                                        <img src="<?php echo bloginfo('template_directory')?>/assets/images/abc.jpg" alt="" class="img-fluid">
                                    </div><!-- end banner-img -->
                                </div><!-- end banner -->
                            </div><!-- end widget -->

                            
                            <div class="widget">
                                <h2 class="widget-title"><?php echo $popular_categories_text; ?></h2>
                                <div class="link-widget">
                                    <ul>
                                    <?php 
                                        $a =get_taxonomies();
                                        $taxonomies = ($a["category"]);
                                        $categories =get_terms( $taxonomies );
                                        foreach($categories as $category){
                                            $term_name =$category->name;
                                            $term_id = $category->term_id;
                                            $term_count = $category->count;
                                            $term_link = get_term_link( $term_id);
                                            ?>
                                            <li><a href="<?php echo $term_link; ?>"><?php echo $term_name; ?><span>(<?php echo $term_count; ?>)</span></a></li>
                                            <?php                                
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>