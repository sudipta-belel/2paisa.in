<?php
function mytheme_function() {
    load_theme_textdomain( 'myfirsttheme', get_template_directory() . '/languages' );
    add_theme_support( 'postformats', array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-logo' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'customize-selective-refresh-widgets' );
}
add_action( 'after_setup_theme', 'mytheme_function' );

function register_my_menus() {
    register_nav_menus(
        array(
        'header-menu' => __( 'Header Menu' ),
        'extra-menu' => __( 'Extra Menu' ),
        )
    );
    }
add_action( 'init', 'register_my_menus' );
require_once('plugins/redux/ReduxCore/framework.php');
require_once('plugins/redux/sample/config.php');
// require_once('plugins/contact-form-7/wp-contact-form-7.php');



    






