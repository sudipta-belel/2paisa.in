<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'test' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '<Yj]h]uc1AMV15F&#>[eTTl:?6bkEioW1q{P<OMtm1:eq`%-p:o,&FzV;~#X.%og' );
define( 'SECURE_AUTH_KEY',  'vmU 4tL]5_t_ju,!-UJi A1P}D}e8ki;m;G J(dnK^Eg8k5J<*uo2ntbLUVplQVA' );
define( 'LOGGED_IN_KEY',    'a# GaiXMS8(9S6|(7OhyJkr>QWER~ldiOn TV7)>e:q>hRpk>?&9;hUlj6C56`/,' );
define( 'NONCE_KEY',        'VrJ})tVZPo(0m0O|K0uZK>h=/zP~iHqh#u(Cj4(X-C5I#M&3T}y255QwHFBf~xB7' );
define( 'AUTH_SALT',        'DtcE.%{^gevfmt?Ip;T/Z9j:taIZ@]*d=<7<X$p?=uJ-A~ ^5vt5vWs}>|a5m~w`' );
define( 'SECURE_AUTH_SALT', 'DBGaNF9%?,uBF)an&)>|I<Q@sZ/+s#YR%4YIRhKbW^_e7Ys=16>]B.FcDe1YB75P' );
define( 'LOGGED_IN_SALT',   '*O8u$|)3hutWyil@uJiFSKJ0>:-_7W29GUn[([!$11teF/sSx3^2#36j%t<V YxI' );
define( 'NONCE_SALT',       'CA,Wm11U3MTq?W+t-;y5m04x;KI*[5Q 4;b^x~.Jd`suSk8X_,XxqE?1+NFl3Cez' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
